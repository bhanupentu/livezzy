import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthConstants } from '../config/auth-constants';
import { HttpService } from './http.service';
import { StorageService } from './storage.service'; 

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpService:HttpService,
    private storageService:StorageService,
    private router:Router
    ) {}

    login(postData :any):Observable<any>{

      console.log('login clicked ')

      return this.httpService.post('/users/authenticate',postData);

    }

    signup(postData :any):Observable<any>{

      return this.httpService.post('/users',postData);

    }

    logout(){
        this.storageService.removeItem(AuthConstants.AUTH).then(res => {

          this.router.navigateByUrl('/login');

        });
    }
}
