import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ConfirmationModalPage } from '../confirmation-modal/confirmation-modal.page';
// import { ViewController } from '@ionic/core';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.page.html',
  styleUrls: ['./modal-form.page.scss'],
})
export class ModalFormPage implements OnInit {
 

  constructor(private router: Router,public modalController: ModalController) { }

  ngOnInit() {
   
  } 
 

  async confirmModal() {

    const modal = await this.modalController.create({
      component: ConfirmationModalPage,
      cssClass: 'modal-container',
      backdropDismiss: true
    });
    this.modalController.dismiss(null, null, 'ModalForm');

    if(!this.payLater.PayLater){
      this.paymentPage();
    }
    else{

      return await modal.present();
    }

  }

  public payLater={
    PayLater 		: true
  }

   paymentPage(){
		this.router.navigateByUrl("/pay");
    }

}
