import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  
  username: string;
  email: string;
	password:	string;

	constructor( 	private router:	Router, 
          private utilService: 	UtilsService,
          private authService: 	AuthService,
          private storageService: 	StorageService ) { }

    ngOnInit() {
    this.username = "";
		this.email 		= "";
		this.password 	= "";
    }

    public postData={
      username: "",
      email 		: "",
		  password 	: ""
    }

    validateInputs(){
      let username = this.postData.email.trim();
      let password = this.postData.password.trim(); 
      let email = this.postData.email.trim();
      return(this.postData.username && 
        this.postData.email && 
        this.postData.password && 
        this.postData.email.length > 0 && 
        this.postData.username.length > 0 && 
        this.postData.password.length > 0);
    }

    signupAction(){ 
        if(this.validateInputs){
        this.authService.signup(this.postData)
        this.router.navigateByUrl("/login") 
        // console.log(this.postData) 
      }else{
        console.log('Invalid Inputs')
      }
    }


}

