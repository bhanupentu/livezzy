import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	email: string;
	password:	string;

	constructor( 	private router:	Router, 
          private utilService: 	UtilsService,
          private authService: 	AuthService,
          private storageService: 	StorageService ) { }

    ngOnInit() {
		this.email 		= "";
		this.password 	= "";
    }

    public postData={
      email 		: "",
		  password 	: ""
    }

    validateInputs(){
      let username = this.postData.email.trim();
      let password = this.postData.password.trim(); 
      return(this.postData.email && this.postData.password && this.postData.email.length > 0 && this.postData.password.length > 0);
    }

    loginAction(){ 
        if(this.validateInputs){
        this.authService.login(this.postData)
        this.router.navigateByUrl("/home") 
        // console.log(this.postData) 
      }else{
        console.log('Invalid Inputs')
      }
    }


}
