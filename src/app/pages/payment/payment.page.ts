import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

	email: 			string;
	password: 		string;

	constructor( 	private router: 		Router, 
					private utilService: 	UtilsService ) { }

    ngOnInit() {
		this.email 		= "";
		this.password 	= "";
    }

    payGateway(){

    // this.router.navigateByUrl("/pay");
    console.log("gateway link .. ");
		
    }

}
